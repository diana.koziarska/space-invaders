const GAME_WIDTH = 800;
const GAME_HEIGTH = 600;

class Pew {
    constructor(source) {
        this.canvas = document.createElement('CANVAS');
        this.canvas.setAttribute("width", 2);
        this.canvas.setAttribute("height", 10);
        this.x = source.originX;
        this.y = source.originY;
        this.m = source.movement;

        const ctx = this.canvas.getContext('2d');
        ctx.fillStyle = 'white';
        ctx.fillRect(0, 0, 2, 10);

        this.move();

    }
    move() {
        this.y += this.m;
        setTimeout(() => this.move(), 100);
    }


    draw(ctx) {
        ctx.drawImage(this.canvas, this.x, this.y);
    }

}


class Ship {
    constructor() {
        this.loaded = false;
        this.image = new Image();
        this.x = 0;
        this.image.addEventListener('load', () => {
            this.loaded = true;
            this.x = GAME_WIDTH / 2 - this.image.width / 2;

        });
        this.image.src = 'assets/Ship.png';
        this.x = GAME_WIDTH / 2;
    }
    set position(value) {
        const maxWidth = GAME_WIDTH - this.image.width;
        this.x = value > maxWidth ? maxWidth : value;
    }
    get originX(){
        return 399;
    }
    get originY(){
        return 500;
    }
    get movement(){
        return -8;
    }

    draw(ctx) {
        ctx.drawImage(this.image, this.x, GAME_HEIGTH - 50);
    }


}


class spaceInvaders {
    constructor() {
        this.ship = new Ship();
        this.fire = [];
        this.canvas = document.createElement('CANVAS');
        this.canvas.setAttribute("width", GAME_WIDTH);
        this.canvas.setAttribute("height", GAME_HEIGTH);
        // this.canvas.addEventListener('mousemove' , e =>{
        //     this.ship.position = e.offsetX;
        //     this.draw();
        // } )

        this.ctx = this.canvas.getContext('2d');

    }


    draw() {
        this.ctx.fillRect(0, 0, GAME_WIDTH, GAME_HEIGTH);
        this.ship.draw(this.ctx);
        this.fire.forEach(fire => fire.draw(this.ctx));
        window.requestAnimationFrame(() => this.draw());
    }

    handleKey(event) {
        console.log(event.code);
        if (event.type === 'keydown') {
            switch (event.code) {
                case 'Space':
                    this.fire.push(new Pew(this.ship));
                    break;

            }
        }

    }

    init() {
        document.body.appendChild(this.canvas);
        this.draw();
    }
}


const game = new spaceInvaders();


window.addEventListener('keydown', event => !event.repeat && game.handleKey(event));
window.addEventListener('keyup', event => !event.repeat && game.handleKey(event));
window.addEventListener('DOMContentLoaded', () => game.init());